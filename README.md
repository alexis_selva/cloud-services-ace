These are the programming assignments relative to cloud services (ACE):

* Assignment 3: understanding of the Wrapper Facade pattern, the Reactor pattern and the Acceptor-Connector pattern in the context of ACE
* Assignment 4: understanding of the Half-Sync/Half-Async pattern in the context of ACE

For more information, I invite you to have a look at https://www.coursera.org/course/mobilecloud