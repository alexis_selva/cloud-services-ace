ATTN: As with all peer-assessments, you will receive the average of your 4 scores.  For the programming assignments, you will receive the maximum score out of the ones you submit (so if you get 35/35 for Java and 20/35 for C++ ACE, you will get 35/35 for Programming Assignment 4).    

The purpose of this assignment is to deepen your understanding of the Half-Sync/Half-Async pattern in the context of ACE . In particular, you will write a platform-independent server program that builds upon the solution from Optional Assignment #3 by using the Half-Sync/Half-Async pattern to accept a connection from one or more clients and echoes back what the client sent. In brackets below are some hints about what kinds of ACE classes/methods you might want to look up to do these (see the ACE Doxygen documentation  and ACE references  for additional details).
In addition to reusing the ACE C++ socket wrappers, ACE_Reactor, and ACE_Acceptor classes from PA#3, you should also use the ACE_Task framework for this assignment (you're also welcome to use C++ 11 features for threading and locking, though you'll have extra work to do since C++ 11 doesn't support some of the concurrency features that ACE does). The other hints are simply for your convenience.

The server program should do the following activities:

Create an Echo_Task that inherits from ACE_Task (configured with the ACE_MT_SYNCH traits class to obtain a synchronized request queue) and Implement its svc() hook method to perform the "half-sync" portion of the server by
Dequeueing messages (ACE_Message_Blocks obtained via ACE_Task::getq()) containing the client input that was put into its synchronized request queue (via the Echo_Svc_Handler::put() method, which in turn calls ACE_Task::putq())
First sends back to the client [ACE_SOCK_Stream] the thread id [ACE_OS::thr_self()] that is running the svc() method 
Then sends back the client's input that was in the message[ACE_SOCK_Stream::sendv_n()].                        
Create an Echo_Svc_Handler that inherits from ACE_Svc_Handler (configured with the ACE_SOCK_STREAM class and ACE_NULL_SYNCH traits class) and implement its handle_input() hook method so that it :
Reads the client data [ACE_SOCK_Stream] until the end of a line is reached (i.e., the symbols "\n", "\r", or "\r\n" are read). 
Puts the client data into a message [ACE_Message_Block], and
Calls Echo_Task::put(), which uses ACE_Task::putq() to enqueue the message for subsequent processing by a thread in the pool of threads that are running the Echo_Task::svc() hook method.        
Create an Echo_Acceptor that inherits from ACE_Acceptor, is instantiated with the Echo_Svc_Handler, uses an Internet domain ``passive-mode'' stream socket to listen a designated port number [ACE_Acceptor, ACE_SOCK_Acceptor, ACE_INET_Addr, etc.].              
Implement a main() function that
Creates an Echo_Task instance and have it spawn a pool of N threads (where N > 1) within itself (Echo_Task::activate()).
Creates an Echo_Acceptor instance and associate it with the Echo_Task.
Creates an  ACE_Reactor (or use the singleton instance of the ACE_Reactor)
Registers the Echo_Acceptor instance with the reactor [ACE_Reactor::register_handler()],
Run the reactor's event loop [ACE_Reactor::run_reactor_event_loop()] to wait for connections/data to arrive from a client.             
When a client connection request arrives, the ACE_Reactor will automatically call the handle_input() method of the ACE_Acceptor. This template method automatically accepts the connection and call the Echo_Svc_Handler::open() hook method to register it with the ACE_Reactor (after the Echo_Acceptor has established the connection and created the Echo_Svc_Handler instance).                
When data arrives from the client the ACE_Reactor will automatically call back on the Echo_Svc_Handler::handle_input() method described above to perform the "half-async" portion of the server.  Note that implementing the half-async portion properly may require multiple trips through the reactor to read each chunk of client data via a single non-blocking recv() each time. 
For this assignment, you can simply exit the server process when you're done, which will close down all the open sockets. 
Make sure to clearly document in your solution which classes play which roles in the Wrapper Facade, Reactor, Acceptor-Connector, and Half-Sync/Half-Async patterns.  

Please implement this server program in C++ using the ACE library (which you can download  here) and contain code in a single file.  A second file may optionally  be included for a simple console client (clearly named) if you write one during your solution development as a test program and wish to include it for the convenience of evaluators, but it is not required  and will not be assessed as part of this exercise, just used as a test driver for your server.  You can also use a telnet client (such as  putty) as a test driver for your server.  

An evaluator should be able to compile it with a command such as "c++ program.cc"  and execute it with './a.out' and your program should successfully run! Please add the used C++ compiler version and implementation (e.g. g++ 4.4, 4.5, clang++ 3.2, 3.3, Intel cc, ...) as well as the version of ACE you used as a comment into the source code.
