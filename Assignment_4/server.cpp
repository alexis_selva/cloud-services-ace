/** 
* Assignment 4: Reactive Webserver (ACE C++)
* ACE version: 6.2.7
* C++ version: 4.6.3
*/


/**
* Dependancies
*/
#include "ace/Acceptor.h"
#include "ace/SOCK_Acceptor.h"


/** 
* Constants
*/
const int THREAD_ID_SIZE = 16;
const int THREAD_POOL_SIZE = 4;
const int CLIENT_TIMEOUT = 30;
const int TASK_TIMEOUT = 30;


/**
 * @class Echo_Task
 * @brief Interface for application message processing (Half-Sync/Half-Async pattern)
 *
 * Create an Echo_Task that inherits from ACE_Task (configured with the ACE_MT_SYNCH 
 * traits class to obtain a synchronized request queue) and Implement its svc() hook
 * method to perform the "half-sync" portion of the server by:
 *   + Dequeueing messages (ACE_Message_Blocks obtained via ACE_Task::getq()) containing 
 *     the client input that was put into its synchronized request queue (via the 
 *     Echo_Svc_Handler::put() method, which in turn calls ACE_Task::putq())
 *   + First sends back to the client [ACE_SOCK_Stream] the thread id [ACE_OS::thr_self()]
 *     that is running the svc() method 
 *   + Then sends back the client's input that was in the message[ACE_SOCK_Stream::sendv_n()].         
 */
class Echo_Task : public ACE_Task < ACE_MT_SYNCH > {

public:
    Echo_Task() {
	activate(THR_BOUND, THREAD_POOL_SIZE);
    }

    virtual int put(ACE_Message_Block *message, ACE_Time_Value *timeout) {
	return putq (message, timeout);
    }

protected:
    virtual int svc () {
	for (ACE_Message_Block *msg ; getq(msg) != -1 ; ) {
    
	    // Extract the client
	    ACE_SOCK_STREAM * peer = (ACE_SOCK_STREAM *) msg->rd_ptr();
	    msg->rd_ptr(sizeof(ACE_SOCK_STREAM));

	    // Send the ID thread to the client
    	    ACE_Thread_ID thread = ACE_Thread_ID();
	    char thread_id[THREAD_ID_SIZE];
	    thread.to_string(thread_id);
	    ssize_t bytes_sent = peer->send(thread_id, strlen(thread_id));

	    // Evaluate the number of bytes sent
	    if (bytes_sent > 0) {

		// Extract the buffer
                char * buffer = msg->rd_ptr();
                ssize_t bytes_to_send = strlen(buffer);

	        // Send the buffer to the client
                bytes_sent = peer->send(buffer, bytes_to_send);

	        // Evaluate the number of bytes sent
                switch (bytes_sent) {
                    case -1:
                        // An error occurred
                        ACE_ERROR_RETURN((LM_ERROR, "(%P|%t) %p\n", "send failed"), -1);
                    case 0:
                        // The peer closed the connection
                        ACE_ERROR_RETURN((LM_ERROR, "(%P|%t) %p\n", "closing socket"), -1);
                    default:
                        ACE_DEBUG((LM_DEBUG, "(%P|%t) to client: %s", buffer));
              	}
	    }
	}
	return 0;
    }
};


/**
* @class Echo_Svc_Handler 
* @brief Service handler using TCP sockets stream
*
* Create an Echo_Svc_Handler that inherits from ACE_Svc_Handler (configured with the 
* ACE_SOCK_STREAM class and ACE_NULL_SYNCH traits class) and implement its handle_input() hook method so that it :
*   + Reads the client data [ACE_SOCK_Stream] until the end of a line is reached (i.e., the symbols "\n", "\r", or "\r\n" are read).
*   + Puts the client data into a message [ACE_Message_Block], and
*   + Calls Echo_Task::put(), which uses ACE_Task::putq() to enqueue the message for 
*      subsequent processing by a thread in the pool of threads that are running the 
*      Echo_Task::svc() hook method.
*/
class Echo_Svc_Handler : public ACE_Svc_Handler < ACE_SOCK_STREAM, ACE_NULL_SYNCH > {

private:
    Echo_Task * echo_task;
    ACE_Time_Value timeout;

public:
    Echo_Svc_Handler() : echo_task(NULL), timeout(0) {} ;
    Echo_Svc_Handler(Echo_Task *task) : echo_task(task), timeout(CLIENT_TIMEOUT) {} ;
    
    // When a client connection request arrives, the ACE_Reactor will automatically call
    // the handle_input() method of the ACE_Acceptor. This template method automatically
    // accepts the connection and call the Echo_Svc_Handler::open() hook method to register
    // it with the ACE_Reactor (after after the Echo_Acceptor has established the connection
    // and created the Echo_Svc_Handler instance).  
    int open(void *) {
	ACE_DEBUG((LM_DEBUG, "(%P|%t) opening socket (fd = %d)\n", this->get_handle()));
        reactor()->register_handler(this, ACE_Event_Handler::READ_MASK);
	reactor()->schedule_timer(this, 0, ACE_Time_Value(CLIENT_TIMEOUT));
	return 0;
    }

protected:
    virtual int handle_timeout(const ACE_Time_Value&, const void *) {
	reactor()->remove_handler(this, ACE_Event_Handler::READ_MASK);
	return 0;
    }

    // When data arrives from the client the ACE_Reactor will automatically call back on the 
    // Echo_Svc_Handler::handle_input() method described above to perform the "half-async" 
    // portion of the server.  Note that implementing the half-async portion properly may 
    // require multiple trips through the reactor to read each chunk of client data via a 
    // single non-blocking recv() each time. 
    virtual int handle_input(ACE_HANDLE) {
	// Read the data sent by the client into a buffer
        char buffer[ACE_DEFAULT_MAX_SOCKET_BUFSIZ];
	ACE_SOCK_STREAM &socket = this->peer();
        ssize_t bytes_read = socket.recv(buffer, sizeof(buffer));

        // Evaluate the number of bytes read
        switch (bytes_read) {
    	    case -1:
		// An error occurred
		ACE_ERROR_RETURN((LM_ERROR, "(%P|%t) %p\n", "receive failed"), -1);
		reactor()->cancel_timer(this);
		reactor()->remove_handler(this, ACE_Event_Handler::READ_MASK);
	    case 0:
		// The peer closed the connection
		ACE_ERROR_RETURN((LM_ERROR, "(%P|%t) closing socket (fd = %d)\n", this->get_handle()), -1);
		reactor()->cancel_timer(this);
		reactor()->remove_handler(this, ACE_Event_Handler::READ_MASK);
	    default:
		ACE_DEBUG((LM_DEBUG, "(%P|%t) from client: %s", buffer));
                reactor()->cancel_timer(this);
		ACE_Message_Block *msg = NULL;
		buffer[bytes_read] = '\0';

		// Create a message
		ACE_NEW_RETURN
		(msg, ACE_Message_Block(bytes_read + 1 + sizeof(ACE_SOCK_STREAM *)), -1);
		
		// Insert the client to the message
		msg->copy((const char *)&socket, sizeof(ACE_SOCK_STREAM));
		
		// Insert the buffer to the message
		msg->copy((const char *)&buffer, bytes_read + 1);
    
		// Enqueue the message
		echo_task->put(msg, &timeout);
		reactor()->schedule_timer(this, 0, ACE_Time_Value(TASK_TIMEOUT));
        }
	return 0;
    } 
};


/**
* @class Echo_Acceptor (Acceptor pattern)
* @brief Acceptor using TCP sockets stream  
*
* Create an Echo_Acceptor that inherits from ACE_Acceptor  and uses 
* an Internet domain ``passive-mode'' stream socket to listen a designated
* port number [ACE_Acceptor, ACE_SOCK_Acceptor, ACE_INET_Addr, etc.].  
*/
class Echo_Acceptor : public ACE_Acceptor <Echo_Svc_Handler, ACE_SOCK_ACCEPTOR > {
    
private:
    Echo_Task * echo_task;

public:    
    Echo_Acceptor(ACE_SOCK_Acceptor::PEER_ADDR addr, ACE_Reactor * reactor, Echo_Task * task) : ACE_Acceptor < Echo_Svc_Handler, ACE_SOCK_ACCEPTOR > (addr, reactor) {
	echo_task = task;
    }
	
protected:
    virtual int make_svc_handler(Echo_Svc_Handler *&handler) {
	handler = new Echo_Svc_Handler(echo_task);
	return 0;
    }
};


/**
* Main
*/ 
int ACE_TMAIN(int argc, ACE_TCHAR *argv[]) {

    // Read the listening port by command line or use the default one (20002)
    if (argc > 2) {
        ACE_ERROR_RETURN((LM_ERROR, "Usage: %s [port-number]\n", argv[0]), -1);
    }
    u_short port = argc < 2 ? ACE_DEFAULT_SERVER_PORT : ACE_OS::atoi(argv[1]);

    // Create an address object (Facade pattern)
    ACE_SOCK_Acceptor::PEER_ADDR listening_socket (port, INADDR_ANY);

    // Create an echo task
    Echo_Task echo_task;
   
    // Create an echo acceptor and associate it with the echo task
    // Create an ACE reactor (or use the singleton instance of the ACE_Reactor) 
    Echo_Acceptor acceptor(listening_socket, ACE_Reactor::instance(), &echo_task);
    ACE_OS::printf("Listening at port %d\n", port);

    // Run event loop (Reactor pattern)
    ACE_Reactor::instance()->run_reactor_event_loop();

    // Return OK
    return 0;
};

