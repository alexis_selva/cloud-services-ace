/** 
* Assignment 3: Reactive Webserver (ACE C++)
* ACE version: 6.2.7
* C++ version: 4.6.3
*/


/**
* Dependancies
*/
#include "ace/Acceptor.h"
#include "ace/SOCK_Acceptor.h"


/**
* @class Echo_Svc_Handler
* @brief Service handler using TCP sockets stream (using a wrapper facade SOCK_STREAM)
*
* Create an Echo_Svc_Handler that inherits from ACE_Svc_Handler
* and implement its handle_input() hook method so that it echos back the client's input
* either (a) a "chunk" at a time or (b) a "line" at a time (i.e., until
* the symbols "\n", "\r", or "\r\n" are read), rather than a character at a time
* [ACE_Svc_Handler, ACE_SOCK_Stream, etc.].
*/
class Echo_Svc_Handler : public ACE_Svc_Handler < ACE_SOCK_STREAM, ACE_NULL_SYNCH > {

public:

    // When a client connection request arrives, the ACE_Reactor will automatically call
    // the handle_input() method of the ACE_Acceptor. This template method automatically
    // accepts the connection and call the Echo_Svc_Handler::open() hook method to register
    // it with the ACE_Reactor (after after the Echo_Acceptor has established the connection
    // and created the Echo_Svc_Handler instance).  
    int open(void *) {
        return reactor()->register_handler(this, ACE_Event_Handler::READ_MASK);
    }

protected:

    // When data arrives from the client the ACE_Reactor will automatically call back on
    // the Echo_Svc_Handler::handle_input() method, which you will need to write so that
    // it echos the client's input back to the client [ACE_SOCK_Stream].
    virtual int handle_input(ACE_HANDLE) {

        // Read the data sent by the client into a buffer
        char buffer[ACE_DEFAULT_MAX_SOCKET_BUFSIZ];
        ssize_t bytes_read = this->peer().recv(buffer, sizeof(buffer));

        // Evaluate the number of bytes read
        switch (bytes_read) {
          case -1:
              // An error occurred
              ACE_ERROR_RETURN((LM_ERROR, "(%P|%t) %p bad read\n", "receive failed"), -1);
          case 0:
              // The peer closed the connection
              ACE_ERROR_RETURN((LM_ERROR, "(%P|%t) closing socket (fd = %d)\n", this->get_handle()), -1);
          default:
              // Reply to the client with the same data
              buffer[bytes_read] = '\0';
              ACE_DEBUG((LM_DEBUG, "(%P|%t) from client: %s", buffer));
	      ssize_t bytes_sent = this->peer().send(buffer, bytes_read);
              if (bytes_sent < 0) {
                  ACE_ERROR_RETURN((LM_ERROR, "%s\n", "send failed"), -1);
 	      }
        }

        return 0;
    }
};


/**
* @class Echo_Acceptor 
* @brief Acceptor using TCP sockets stream  
*
* Create an Echo_Acceptor that inherits from ACE_Acceptor (or uses a simple typedef) and uses 
* an Internet domain ``passive-mode'' stream socket to listen a designated
* port number [ACE_Acceptor, ACE_SOCK_Acceptor, ACE_INET_Addr, etc.].  
*/
typedef ACE_Acceptor < Echo_Svc_Handler, ACE_SOCK_ACCEPTOR > Echo_Acceptor;


/**
* Main
*/ 
int ACE_TMAIN(int argc, ACE_TCHAR *argv[]) {

    // Read the listening port by command line or use the default one (20002)
    if (argc > 2) {
        ACE_ERROR_RETURN((LM_ERROR, "Usage: %s [port-number]\n", argv[0]), -1);
    }
    u_short port = argc < 2 ? ACE_DEFAULT_SERVER_PORT : ACE_OS::atoi(argv[1]);

    // Create an address object using the wrapper facade
    ACE_SOCK_Acceptor::PEER_ADDR listening_socket (port, INADDR_ANY);
    
    // Create an ACE_Reactor (or use the singleton instance of the ACE_Reactor), register the 
    // Echo_Acceptor instance with it [ACE_Reactor::register_handler()], and use its event 
    // loop [ACE_Reactor::run_reactor_event_loop()] to wait for a connection to arrive from a client.
    Echo_Acceptor acceptor(listening_socket, ACE_Reactor::instance());
    ACE_OS::printf("Listening at port %d\n", port);
    ACE_Reactor::instance()->run_reactor_event_loop();

    // Return OK
    return 0;
};

